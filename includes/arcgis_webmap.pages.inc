<?php
/**
 * @file
 * Code for arcgis_webmap.pages.inc.
 */

/**
 * Page callback: geocode page.
 */
function arcgis_webmap_geocode_page() {
  $query = drupal_get_query_parameters();
  $address = isset($query['address']) ? drupal_json_encode($query['address']) : '{}';
  $arcgis_geocode_url = isset($query['arcgis_geocode_url']) ? check_plain($query['arcgis_geocode_url']) : 'https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer';
  $basemap = isset($query['basemap']) ? check_plain($query['basemap']) : 'streets';
  $page = array(
    '#theme' => 'arcgis_geocode',
    '#address' => $address,
    '#arcgis_geocode_url' => $arcgis_geocode_url,
    '#basemap' => $basemap,
  );

  print drupal_render($page);
  drupal_exit();
}
