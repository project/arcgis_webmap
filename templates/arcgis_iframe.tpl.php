<?php
/**
 * @file
 * Theme implementation to display an ArcGIS geocoded iframe wrapper.
 */
?>
<div class="arcgis-iframe-wrapper <?php print $aspect ?>">
    <iframe class="<?php print $classes ?>" height="100%" width="100%" scrolling="no" src="<?php print $iframe_path ?>"></iframe>
</div>
