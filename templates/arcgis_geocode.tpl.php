<?php

/**
 * @file
 * Theme implementation to display an ArcGIS geocoded addressfield.
 */
?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <!--The viewport meta tag is used to improve the presentation and behavior of the samples
    on iOS devices-->
  <meta name="viewport" content="initial-scale=1, maximum-scale=1,user-scalable=no">
  <title>ArgGIS Geocode</title>

  <link rel="stylesheet" href="https://js.arcgis.com/3.13/dijit/themes/claro/claro.css">
  <link rel="stylesheet" href="https://js.arcgis.com/3.13/esri/css/esri.css">
  <style>
    html, body {
      height: 100%; width: 100%;
      margin: 0; padding: 0;
    }
    #map{
      padding:0;
      border: solid 1px #999;
      -webkit-border-radius: 3px;
      -moz-border-radius: 3px;
      border-radius: 3px;
      -moz-box-shadow: 0 0 5px #888;
      -webkit-box-shadow: 0 0 5px #888;
      box-shadow: 0 0 5px #888;
    }
  </style>
  <script src="https://js.arcgis.com/3.13compact/"></script>
  <script>
    var map, locator;

    require([
      "esri/map", "esri/tasks/locator", "esri/graphic",
      "esri/dijit/Popup", "esri/dijit/PopupTemplate",
      "esri/InfoTemplate", "esri/symbols/SimpleMarkerSymbol",
      "esri/symbols/Font", "esri/symbols/TextSymbol",
      "dojo/_base/array", "esri/Color",
      "dojo/number", "dojo/parser", "dojo/dom",
      "dojo/dom-construct",

      "dijit/layout/BorderContainer", "dijit/layout/ContentPane", "dojo/domReady!"
    ], function(
      Map, Locator, Graphic,
      Popup, PopupTemplate,
      InfoTemplate, SimpleMarkerSymbol,
      Font, TextSymbol,
      arrayUtils, Color,
      number, parser, dom,
      domConstruct
    ) {
      parser.parse();

      var popup = new Popup({
        titleInBody: true
      }, domConstruct.create("div"));

      map = new Map("map", {
        basemap: "<?php print $basemap ?>",
        center: [-93.5, 41.431],
        zoom: 3,
        infoWindow: popup
      });

      locator = new Locator("<?php print $arcgis_geocode_url ?>");
      locator.on("address-to-locations-complete", showResults);

      map.infoWindow.resize(200,125);

      function locate() {
        locator.outSpatialReference = map.spatialReference;
        var options = {
          address: <?php print $address ?>,
          outFields: ["Loc_name"],
          maxLocations: 1
        };
        locator.addressToLocations(options);
      }

      function showResults(evt) {
        var symbol = new SimpleMarkerSymbol({
          "color": [0,0,255,180],
          "size": 8,
          "type": "esriSMS",
          "style": "esriSMSCircle",
          "outline": {
            "color": [105,105,105,70],
            "width": 1,
            "type": "esriSLS",
            "style": "esriSLSSolid"
          }
        });
        var infoTemplate = new InfoTemplate({
          title: 'Location',
          content:"${address}"
        });

        var geom;
        arrayUtils.every(evt.addresses, function(candidate) {
          if (candidate.score > 80) {
            var attributes = {address: candidate.address};
            geom = candidate.location;

            var graphic = new Graphic(geom, symbol, attributes, infoTemplate);
            // Add a graphic to the map at the geocoded location.
            map.graphics.add(graphic);
            // Break out of loop after one candidate with score greater  than 80 is found.
            return false;
          }
        });
        if ( geom !== undefined ) {
          map.centerAndZoom(geom, 15);
        }
      }

      function errBack(error) {
        console.log("Error - "+error)
        console.log("Error code - "+error.code)
      }

      // On load, geocode.
      locate();
    });
  </script>
</head>
<body class="claro">
<div id="mainWindow" data-dojo-type="dijit/layout/BorderContainer"
  data-dojo-props="design:'sidebar', gutters:false"
  style="width:100%; height:100%;">

  <div id="map"
    data-dojo-type="dijit/layout/ContentPane"
    data-dojo-props="region:'center'">
  </div>
</div>
</body>
</html>
