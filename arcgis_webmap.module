<?php

/**
 * @file
 * Displays a geocoded map from ArcGIS.
 */

/**
 * Implements hook_menu().
 */
function arcgis_webmap_menu() {
  $items = array();

  $items['arcgis_geocode'] = array(
    'title' => 'ArgGIS Geocode',
    'page callback' => 'arcgis_webmap_geocode_page',
    'access callback' => TRUE,
    'file' => 'arcgis_webmap.pages.inc',
    'file path' => drupal_get_path('module', 'arcgis_webmap') . '/includes',
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implements hook_field_info().
 */
function arcgis_webmap_field_info() {
  return array(
    'arcgis_webmap' => array(
      'label' => t('ArcGIS webmap'),
      'description' => t('ArcGIS field that will render a webmap.'),
      'default_widget' => 'webmapid',
      'default_formatter' => 'webmap_formatter',
      'settings' => array(
        'arcgis_url' => 'https://www.arcgis.com',
        'arcgis_theme' => 'light',
      ),
      // Support hook_entity_property_info() from contrib "Entity API".
      'property_type' => 'arcgis_webmap',
      'property_callbacks' => array('arcgis_webmap_field_property_info_callback'),
    ),
  );
}

/**
 * Implements hook_field_load().
 */
function arcgis_webmap_field_load($entity_type, $entities, $field, $instances, $langcode, &$items, $age) {
  foreach ($entities as $id => $entity) {
    foreach ($items[$id] as $delta => $item) {
      $items[$id][$delta]['data'] = _arcgis_webmap_data_load($field, $item, $instances[$id]);
    }
  }
}

/**
 * Unpacks the item data for use.
 */
function _arcgis_webmap_data_load($field, $item, $instance) {
  if (isset($item['data'])) {
    if (!is_array($item['data'])) {
      $item['data'] = unserialize($item['data']);
    }
    return $item['data'];
  }
}

/**
 * Implements hook_field_settings_form().
 */
function arcgis_webmap_field_settings_form($field, $instance, $has_data) {
  return array(
    'arcgis_url' => array(
      '#type' => 'textfield',
      '#title' => t('ArcGIS URL'),
      '#description' => t('ArcGIS.com or ArcGIS.com Subscription or Portal for ArcGIS URL.'),
      '#default_value' => $field['settings']['arcgis_url'],
      '#required' => TRUE,
    ),
    'arcgis_theme' => array(
      '#type' => 'textfield',
      '#title' => t('ArcGIS color theme'),
      '#description' => t('ArcGIS.com or ArcGIS.com color theme i.e. light, dark.'),
      '#default_value' => $field['settings']['arcgis_theme'],
      '#required' => TRUE,
    ),
  );
}

/**
 * Implements hook_field_validate().
 */
function arcgis_webmap_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
  foreach ($items as $delta => $item) {
    if (preg_match('/^[a-z0-9]$/', $item['mapid'])) {
      $errors[$field['field_name']][$langcode][$delta][] = array(
        'error' => 'arcgis_validation',
        'message' => t('%name: the map id uses invalid characters. It only accepts alphanumeric.', array('%name' => $instance['label'])),
      );
    }
  }
}

/**
 * Implements hook_field_is_empty().
 */
function arcgis_webmap_field_is_empty($item, $field) {
  if (empty($item['mapid'])) {
    return TRUE;
  }

  return FALSE;
}

/**
 * Additional callback to adapt the property info of arcgis_webmap fields.
 *
 * @see entity_metadata_field_entity_property_info()
 */
function arcgis_webmap_field_property_info_callback(&$info, $entity_type, $field, $instance, $field_type) {
  $property = &$info[$entity_type]['bundles'][$instance['bundle']]['properties'][$field['field_name']];

  $property['type'] = 'struct';
  $property['getter callback'] = 'entity_metadata_field_verbatim_get';
  $property['setter callback'] = 'entity_metadata_field_verbatim_set';
  $property['property info']['mapid'] = array(
    'type' => 'token',
    'label' => t('The webmap map id.'),
    'getter callback' => 'entity_metadata_field_property_get',
    'setter callback' => 'entity_property_verbatim_set',
    'raw getter callback' => 'entity_property_verbatim_get',
  );
  $property['property info']['data'] = array(
    'type' => 'struct',
    'label' => t('Metadata data about the map.'),
    'setter callback' => 'entity_property_verbatim_set',
    'getter callback' => 'arcgis_webmap_data_property_get',
  );
}

/**
 * Entity property info getter callback for arcgis_webmap attributes.
 */
function arcgis_webmap_data_property_get($data, array $options, $name, $type, $info) {
  return isset($data[$name]) ? $data[$name] : NULL;
}

/**
 * Implements hook_field_widget_info().
 */
function arcgis_webmap_field_widget_info() {
  return array(
    'mapid' => array(
      'label' => t('ArcGIS Map by ID'),
      'field types' => array('arcgis_webmap'),
    ),
  );
}

/**
 * Implements hook_field_widget_form().
 */
function arcgis_webmap_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $widget = $element;
  $widget['#delta'] = $delta;
  $widget += array(
    '#type' => 'fieldset',
    '#title' => t('ArcGIS Webmap ID'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  switch ($instance['widget']['type']) {
    case 'mapid':
      $widget['mapid'] = array(
        '#type' => 'textfield',
        '#title' => t('Map ID'),
        '#default_value' => isset($items[$delta]['mapid']) ? $items[$delta]['mapid'] : '',
        '#delta' => $delta,
        '#size' => 64,
        '#description' => 'Add the map id.',
        '#maxlength' => 64,
        '#required' => $instance['required'],
      );
      $widget['data']['aspect'] = array(
        '#type' => 'radios',
        '#title' => t('Aspect Ratio'),
        '#options' => array('4x3' => t('4x3 (landscape)'), '3x4' => t('3x4 (portrait)')),
        '#default_value' => isset($items[$delta]['data']['aspect']) ? $items[$delta]['data']['aspect'] : '4x3',
        '#description' => t('Aspect ratio of map.'),
        '#delta' => $delta,
      );

      // Map display options.
      $options = array(
        'legend' => t('Display a legend'),
        'details' => t('Display map details'),
        'zoom' => t('Display +/- zoom navigation buttons'),
        'home' => t('Display a Home button to return to default zoom and centering (only functional with zoom navigation buttons)'),
      );
      $context = array(
        'form' => &$form,
        'form_state' => &$form_state,
        'field' => $field,
        'instance' => $instance,
        'langcode' => $langcode,
        'items' => $items,
        'delta' => $delta,
        'element' => $element,
      );
      // Allow additional display options.
      drupal_alter('arcgis_webmap_display_options', $options, $context);

      $widget['data']['options'] = array(
        '#type' => 'checkboxes',
        '#title' => t('Map display options'),
        '#options' => $options,
        '#default_value' => isset($items[$delta]['data']['options']) ? $items[$delta]['data']['options'] : array(),
        '#delta' => $delta,
      );

      break;
  }

  return $widget;
}


/**
 * Implements hook_field_insert().
 */
function arcgis_webmap_field_insert($entity_type, $entity, $field, $instance, $langcode, &$items) {
  foreach ($items as $delta => $value) {
    // Serialize the data array.
    if (!is_string($items[$delta]['data'])) {
      $items[$delta]['data'] = serialize($items[$delta]['data']);
    }
  }
}


/**
 * Implements hook_field_update().
 */
function arcgis_webmap_field_update($entity_type, $entity, $field, $instance, $langcode, &$items) {
  arcgis_webmap_field_insert($entity_type, $entity, $field, $instance, $langcode, $items);
}

/**
 * Implements hook_field_widget_error().
 */
function arcgis_webmap_field_widget_error($element, $error, $form, &$form_state) {
  form_error($element, $error['message']);
}

/**
 * Implements hook_field_formatter_info().
 */
function arcgis_webmap_field_formatter_info() {
  return array(
    'arcgis_webmap_id' => array(
      'label' => t('Webmap'),
      'field types' => array('arcgis_webmap'),
    ),
    'arcgis_geocode' => array(
      'label' => t('ArcGIS geocode address'),
      'field types' => array('addressfield'),
      'settings' => array(
        'arcgis_geocode_url' => 'https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer',
        'basemap' => 'streets',
      ),
    ),
  );
}


/**
 * Implements hook_field_formatter_settings_form().
 */
function arcgis_webmap_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $element = array();

  switch ($field['module']) {
    case 'addressfield':
      $element['arcgis_geocode_url'] = array(
        '#type' => 'textfield',
        '#title' => t('Geocode endpoint.'),
        '#default_value' => !empty($instance['display'][$view_mode]['settings']['arcgis_geocode_url']) ? $instance['display'][$view_mode]['settings']['arcgis_geocode_url'] : '',
      );

      // Map basemap options.
      $options = array(
        'dark-gray' => t('Dark gray'),
        'gray' => t('Gray'),
        'hybrid' => t('Hybrid'),
        'national-geographic' => t('National Geographic'),
        'oceans' => t('Oceans'),
        'osm' => t('OpenStreetMap'),
        'satellite' => t('Satellite'),
        'streets' => t('Streets'),
        'terrain' => t('Terrain'),
        'topo' => t('Topo'),
      );
      $context = array(
        'field' => $field,
        'instance' => $instance,
        'view_mode' => $view_mode,
        'form' => $form,
        'form_state' => &$form_state,
      );
      // Allow additional basemaps.
      drupal_alter('arcgis_webmap_basemap', $options, $context);
      asort($options);

      $default_value = !empty($instance['display'][$view_mode]['settings']['basemap']) ? $instance['display'][$view_mode]['settings']['basemap'] : 'streets';
      $element['basemap'] = array(
        '#type' => 'select',
        '#title' => t('Basemap'),
        '#options' => $options,
        '#description' => t('The basemap defaults to streets, but other options exist.'),
        '#default_value' => $default_value,
      );

      break;
  }

  return $element;
}

/**
 * Implements hook_field_formatter_view().
 */
function arcgis_webmap_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();

  switch ($display['type']) {
    case 'arcgis_webmap_id':
      foreach ($items as $delta => $item) {
        if (!empty($item['mapid'])) {
          $arcgis_url_params = array();
          foreach ($item['data']['options'] as $key => $value) {
            if (!empty($value)) {
              $arcgis_url_params['query'][$key] = 'true';
            }
            else {
              $arcgis_url_params['query'][$key] = 'false';
            }
          }
          // Set additional required parameters.
          $arcgis_url_params['query']['theme'] = $field['settings']['arcgis_theme'];
          $arcgis_url_params['query']['webmap'] = $item['mapid'];
          $arcgis_url_params['external'] = TRUE;

          $arcgis_url = check_url(url($field['settings']['arcgis_url'], $arcgis_url_params));
          $element[$delta] = array(
            '#theme' => 'arcgis_iframe',
            '#iframe_path' => $arcgis_url,
            '#aspect' => $item['data']['aspect'],
            '#attached' => array(
              'css' => array(
                drupal_get_path('module', 'arcgis_webmap') . '/css/arcgis_webmap_iframe.css',
              ),
            ),
          );
        }
      }
      break;

    case 'arcgis_geocode':
      foreach ($items as $delta => $item) {
        $components = array(
          'Address' => 'thoroughfare',
          'Neighborhood' => 'dependent_locality',
          'City' => 'locality',
          'Region' => 'administrative_area',
          'Subregion' => 'sub_administrative_area',
          'Postal' => 'postal_code',
          'CountryCode' => 'country',
        );
        $address = array();
        foreach ($components as $esri => $addressfield) {
          if (!empty($item[$addressfield])) {
            $address[$esri] = $item[$addressfield];
          }
        }

        $arcgis_geocode_url = url('arcgis_geocode', array(
          'query' => array(
            'address' => $address,
            'arcgis_geocode_url' => $display['settings']['arcgis_geocode_url'],
            'basemap' => $display['settings']['basemap'],
          ),
        ));

        $element[$delta] = array(
          '#theme' => 'arcgis_iframe',
          '#iframe_path' => $arcgis_geocode_url,
          '#attached' => array(
            'css' => array(
              drupal_get_path('module', 'arcgis_webmap') . '/css/arcgis_webmap_iframe.css',
            ),
          ),
        );
      }
      break;
  }

  return $element;
}

/**
 * Implements hook_theme().
 */
function arcgis_webmap_theme(array $existing, $type, $theme, $path) {
  $path = $path . '/templates';

  return array(
    'arcgis_geocode' => array(
      'template' => 'arcgis_geocode',
      'path' => $path,
      'variables' => array(
        'address' => NULL,
        'arcgis_geocode_url' => NULL,
        'basemap' => NULL,
      ),
    ),
    'arcgis_iframe' => array(
      'template' => 'arcgis_iframe',
      'path' => $path,
      'variables' => array(
        'iframe_path' => NULL,
        'aspect' => NULL,
      ),
    ),
  );
}

/**
 * Implements hook_preprocess_HOOK().
 */
function arcgis_webmap_preprocess_arcgis_iframe(&$variables) {
  // Prefix the css classes appropriately.
  if ($variables['aspect']) {
    $variables['aspect'] = 'arcgis-aspect arcgis-' . $variables['aspect'];
  }
}
